
    <div class="container">
        <div class="card">
            <div class="card-header">
                Articles <small>({{ $articles->count() }})</small>
                <a href="{{ route('article.create') }}">Add article</a>
            </div>
            <div class="card-body">
                <form action="{{ route('article.index') }}" method="get">
                    <div class="form-group">
                        <input
                            type="text"
                            name="search"
                            class="form-control"
                            placeholder="Search..."
                            value="{{ request('search') }}"
                        />
                    </div>
                </form>

                @forelse ($articles as $article)
                    <article class="mb-3">
                        <strong>{{ $article->id }}</strong>
                        <form action="{{ route('article.delete', ['id' => $article->id]) }}" method="post">
                            <input class="btn btn-default" type="submit" value="Delete" />
                            @csrf
                            <input type="hidden" name="_method" value="delete" />
                        </form>
                        <h2>{{ $article->title }}</h2>
                        <p class="m-0">{{ $article->body }}</body>
                        <div>
                            @foreach ($article->tags as $tag)
                                <span class="badge badge-light">{{ $tag}}</span>
                            @endforeach
                        </div>


                    </article>
                    <hr>
                @empty
                    <p>No articles found</p>
                @endforelse

                {{ $articles->links() }}
            </div>
        </div>
    </div>
