<form action="{{ route('article.store') }}" method="post">
    @csrf
    <div>
        <label for="title">Title</label>
        <input type="text" id="title" name="title" placeholder="Title">
    </div>
    <div>
        <label for="body">Body</label>
        <textarea name="body" id="dody"></textarea>
    </div>
    <div>
        <label for="tags">Tags</label>
        <input type="text" id="tags" name="tags" placeholder="Tags">
        <p>Separate tags by comma</p>
    </div>

    <input class="btn btn-default" type="submit" value="Add articles" />

</form>
