<?php

namespace App\Repositories;
use App\Models\Article;
use Elasticsearch\ClientBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class ArticlesRepository
{

    /**
     * ElasticSearch builder object
     * @var \Elasticsearch\ClientBuilder
     */
    private $elasticsearch;

    public function __construct(ClientBuilder $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch::create()->build();
    }

    /**
     * Search method
     * @param string $query - search params
     * @return Collection
     */
    public function search(string $query = ''): Collection
    {
        if (config('services.search.enabled')) {
            return $this->searchByElasticSearch($query);
        }

        return $this->searchByEloquent($query);
    }

    /**
     * Search by ElasticSearch Engine
     * @param string $query - search params
     * @return Collection
     */
    private function searchByElasticSearch(string $query = ''): Collection
    {
        $model = new Article;

        $items = $this->elasticsearch->search([
            'index' => $model->getTable(),
            'type' => $model->getTable(),
            'body' => [
                'query' => [
                    'multi_match' => [
                        'fields' => ['title^5', 'body', 'tags'],
                        'query'  => $query,
                    ],
                ],
            ],
        ]);

        $prepareRecords = [];

        foreach ($items['hits']['hits'] as $item) {
            array_push($prepareRecords, (object) $item['_source']);
        }

        return new Collection($prepareRecords);

    }

    /**
     * Search by default Eloquent ORM
     * @param string $query
     * @return Collection
     */
    private function searchByEloquent(string $query = ''): Collection
    {
        return Article::query()
            ->where('body', 'like', "%{$query}%")
            ->orWhere('title', 'like', "%{$query}%")
            ->get();
    }
}

