<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Repositories\ArticlesRepository;



class ArticleController extends Controller
{

    /**
     * Page with articles list
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (isset($request->search)) {
            $articles = Article::search($request->search)->select(['title', 'tags'])->paginate(5);

        } else {
            $articles = Article::paginate(5);
        }

        return view('articles.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.form');
    }

    /**
     * Store a new created resource in storage.
     * @param Request $request
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Article $article)
    {
        $input = $request->all();
        $input['tags'] = explode(',',  $input['tags']);

        $article->fill($input);
        $article->save();

        return redirect()->route('article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $article = Article::findOrFail($id);

        $article->delete();

        return redirect()->route('article.index');
    }



}
