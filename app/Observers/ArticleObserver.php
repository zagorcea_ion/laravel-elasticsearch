<?php

namespace App\Observers;

use App\Models\Article;
use Elasticsearch\ClientBuilder;

class ArticleObserver
{
    /**
     * ElasticSearch builder object
     * @var \Elasticsearch\ClientBuilder
     */
    private $elasticsearch;

    public function __construct(ClientBuilder $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch::create()->build();
    }


    /**
     * Handle the article "saved" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function saved(Article $article)
    {
//        $this->elasticsearch->index([
//            'index' => $article->getTable(),
//            'type'  => $article->getTable(),
//            'id'    => $article->getKey(),
//            'body'  => $article->toArray(),
//        ]);
    }

    /**
     * Handle the article "deleted" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function deleted(Article $article)
    {
        $this->elasticsearch->delete([
            'index' => $article->getTable(),
            'type'  => $article->getTable(),
            'id'    => $article->getKey(),
        ]);
    }

    /**
     * Handle the article "created" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function created(Article $article)
    {
        //
    }

    /**
     * Handle the article "updated" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function updated(Article $article)
    {
        //
    }

    /**
     * Handle the article "restored" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function restored(Article $article)
    {
        //
    }

    /**
     * Handle the article "force deleted" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function forceDeleted(Article $article)
    {
        //
    }
}
