<?php

namespace App\Models;

use App\ArticlesIndexConfigurator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;


class Article extends Model
{
    use HasFactory;
    use Searchable;

    protected $indexConfigurator = ArticlesIndexConfigurator::class;

    protected $mapping = [
        'properties' => [
            'title' => [
                'type' => 'text',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                    ]
                ]
            ],
        ]
    ];

    /**
     * Define casts that transform tags array in JSON format
     *
     * @var array
     */
    protected $casts = [
        'tags' => 'json',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'body', 'tags'];
}
