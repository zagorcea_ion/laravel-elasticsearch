<?php

namespace App\Console\Commands;

use App\Models\Article;
use Elasticsearch\ClientBuilder;
use Illuminate\Console\Command;

class ElasticsearcReindexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elasticsearch:reindex';

    /** @var \Elasticsearch\Client */
    private $elasticsearch;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexes all articles to Elasticsearch';

    /**
     * Create a new command instance.
     *
     * @param ClientBuilder $elasticsearch
     */
    public function __construct(ClientBuilder $elasticsearch)
    {
        parent::__construct();
        $this->elasticsearch = $elasticsearch::create()->build();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Indexing all articles. This might take a while...');
        foreach (Article::get() as $article)
        {
            $this->elasticsearch->index([
                'index' => $article->getTable(),
                'type'  => $article->getTable(),
                'id'    => $article->getKey(),
                'body'  => $article->toArray(),
            ]);

            $this->output->write('.');
        }

        $this->info('\nDone!');
    }
}
