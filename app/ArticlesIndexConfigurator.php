<?php

namespace App;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class ArticlesIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'articles';

    /**
     * @var array
     */
    protected $settings = [
        //
    ];
}
